该三方开源库从github fork过来

fork地址：https://github.com/woxingxiao/XEditText

# XEditText

XEditText: clear contents easily; show/hide password; separeta contents automatically, such as credit card number; disable Emoji inputs; 带删除功能的EditText；显示或者隐藏密码；可设置自动添加分隔符分割电话号码、银行卡号等；支持禁止Emoji表情符号输入。

效果展示

<img src="https://gitee.com/openharmony-tpc/XEditText/raw/master/XEditext1.gif" width="40%"/>

<img src="https://gitee.com/openharmony-tpc/XEditText/raw/master/XEditext2.gif" width="40%"/>        





添加依赖：

方式一： 添加har包到lib文件夹内 在entry的gradle内添加如下代码 implementation fileTree(dir: 'libs', include: ['.jar', '.har'])

方式二： allprojects { repositories{ mavenCentral() } } 

implementation 'io.openharmony.tpc.thirdlib:XEditText:1.0.0'

Usage

<com.xw.repo.XEditText
     ohos:id="$+id:default_edit_text"
     ohos:width="match_parent"
     ohos:height="match_content"
     ohos:hint="default, just like EditText"
     app:x_disableClear="true"
      ohos:text_size="14fp"/>
<com.xw.repo.XEditText
     ohos:id="$+id:default_clear_text"
     ohos:width="match_parent"
     ohos:height="match_content"
     ohos:text_size="14fp"
     ohos:hint="clear drawable"/>

 <com.xw.repo.XEditText
     ohos:id="$+id:clear_marker_edit_text"
     ohos:width="match_parent"
     ohos:height="match_content"
     ohos:text_input_type="pattern_password"
     ohos:text_size="14fp"
      ohos:hint="default password input" />


      License
      The MIT License (MIT)
    
      Copyright (c) 2016 woxingxiao
    
      Permission is hereby granted, free of charge, to any person obtaining a copy
      of this software and associated documentation files (the "Software"), to deal
      in the Software without restriction, including without limitation the rights
      to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
      copies of the Software, and to permit persons to whom the Software is
      furnished to do so, subject to the following conditions:
    
      The above copyright notice and this permission notice shall be included in all
      copies or substantial portions of the Software.
    
      THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
      IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
      FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
      AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
      LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
      OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
      SOFTWARE.